title: >-
  Conceptos, historia y desafíos en agentes de diálogo desde una óptica
  cognitivo-conductual.
author: Patricio Julian Gerpe
tags: []
categories: []
date: 2018-11-05 19:56:00
---
Abstracto:
El propósito de este artículo consiste en (A) profundizar a los lectores en nociones básicas de desarrollo de agentes conversacionales analizando estos conceptos desde una analogía con la psicología cognitiva y conductual, (B) brindar un pantallazo histórico sobre dicha tecnología y (C ) mencionar los desafíos contemporáneos que existen en el área en respuesta a cambios históricos.
Introducción:
Este artículo argumento que la psicología conductual y cognitiva sirven de base de estudio para el desarrollo de agentes de diálogo dado que para que nuestro agente se comunique efectivamente con las personas es indispensable emular comportamientos humanos procesando estímulos externos para la toma de decisiones comportamentales adaptativas, y ambos aspectos son estudiados profundamente por dichas disciplinas en cuestión. En primer lugar, explicaré las nociones básicas en torno al desarrollo de agentes de diálogo, consecuentemente resumiré la historia de las ciencias cognitivas y el internet, marcando hitos que ayudaron al desarrollo y finalmente compartiré mi propio resumen de los desafíos contemporáneos que visualice en el área en respuesta a dichos cambios históricos.
A — Retomando nociones básicas:
Asimismo, se puede conocer a los agentes como ChatterBot, termino originalmente propuesto por Micheal Mauldin para describir dichos programas conversacionales en 1994, tras la creación de su bot “Julia”, ganador del premio Loebner.
Amir Shevat (2017), en su remarca que los bots son la interfaz de un servicio y no un servicio en si mismo al definir a los chatbots.
¿Bot o chatbot?
Bot: Un bot es una pieza de software cuyo objetivo principal reside en la automatización de tareas repetitivas.
Chatbot: Un chatbot es un bot cuya interfaz de interacción con el usuario para cometer dicho fin es un chat, o mejor dicho, una plataforma de diálogo virtual.
Otras interfaces <-> Tecnologías de comunicación
Un agente de dialogo precisa de un medio para comunicarse con un usuario humano. Este proceso de interacción es facilitado por la tecnologías de comunicación, las cuáles pueden ser categorizadas en diferentes interfaces:
Por ejemplo:
Interfaz de voz: (Ejem Siri, Alexa, Google Home)
Cognición embebida: Robots (Ejem: Pepper, nao)
Interfaz web -> Interfaz mobile -> Interfaz de chat
Robot vs Softbot
En primer lugar, se entiende por robot a un sistema electromecánico programable capaz de realizar operaciones repetitivas de manera autónoma.
Un robot, es normalmente entendido como humanoide cuando: (A) este tiene una apariencia visiblemente similar a la de un humano y (B) este emula comportamientos típicos de ellos.
Hiroshi Ishiguro, director del laborotorio de robótica inteligente de la Universidad de Osaka argumenta que los robots deberían tener un aspecto humanoide dado a que, como seres sociales, el humano posee un cerebro preparado para comprender y responder ante el comportamiento de otros humanos.
El softbot, en cambio, es un sistema informático sin hardware directamente correlacionado. (Ejemplo: Poncho Bot de Facebook Messenger).
Interacción Humano-Computador
En función de desarrollar un sistema de diálogo que efectivamente pueda procesar y abordar las consultas de su usuarios es importante adoptar un abordaje centrado en la experiencia de usuario. Existe una disciplina especialmente destinada al estudio del intercambio de información entre las personas y las computadoras y a esta se la denomina “Interacción-Humano-Computador” o IHC.
En la actualidad, se suele hablar de IA con diseño centrado en el humano, en referencía al desarrollo de plataformas de interacción usables, utiles y usadas. (3 ‘U’ del diseño de experiencia de usuario).
Acercamiento al concepto: Ingenieria de la usabilidad. (Este término es un antecesor de dicho concepto).
En un sistema de diálogo virtual, cada unidad de intercambio informativo entre persona(usuario) y la computadora(agente), se las denomina como “nodo de diálogo”, “nodo de conversación” o simplemente “interacción”.
Por un lado, la información que envíe el usuario(la persona) se la puede conocer como “declaración”, “consulta” o “uteración” como anglicismo de la palabra inglesa “utterance”. Dicha información será un “input” para nuestro sistema.
Este input puede ser procesado por nuestro agente, a grandes rasgos, de dos maneras, de las cuales, a su vez, de la segunda se desprenden cuatro diferentes sub-enfoques:
1- Conductualmente
A- Enfoque por patrones. (Ejem: AIML — GAIML)
2- Cognitivamente
A- Enfoque por intenciones. (Ejem: DialogFlow, Watson Conversation)
B- Enfoque por entidades. (Ejem:Luis.AI, OpenNLP)
C- Enfoque por análisis léxico. (Ejem: Andybot, Alchemy)
D- Enfoque mixto
Ahora bien, ¿Por qué realizo esta clasificación en el enfoque i/o es conductual? ¿y qué es “Cognición”?
El conductismo es una corriente de pensamiento e investigación dentro del campo de la psicología que se enfoca en el estudio de las conductas observables. Uno de los conceptos más importantes del conductismo es el de ‘caja negra’, que conceptualiza la elaboración de conductas en humanos como un proceso reactivo. A cierto estimulo, cierta conducta asociada, no hay noción de ‘mente’. Necesitamos del estudio de las conductas humanas adaptativas para poder emularlas en un modelo computacional.
Análogamente, en el caso de lenguaje de diálogo “AIML”, basado en xml, se programa agentes en base a inputs reales de usuario, y cada input (en este caso se denomina patrón”) se devuelve un determinado repertorio de respuestas.
I → O
|Concepto de CAJA NEGRA|
En contraposición, la psicología de enfoque cognitivo emerge en oposición al concepto de “caja negra”, para los cognitivistas existe un proceso de procesamiento de la información recibida del ambiente que modula la conducta humana. En estos casos, esos son los procesos que se intentan emular en los enfoques cognitivos de desarrollo de agentes de diálogo y a esto se lo comprende como “cognición”.
I → PROCESAMIENTO → O
El cognitivismo ha estudiado y estudia los siguientes procesos mentales:
Los procesos mentales básicos
Percepción: La capacidad de transmutar sensorialmente información del ambiente proveniente de distintos tipos de fuentes (energía química desde el sentido nasal y del gusto, energía física desde el sentido auditivo y visual, etc.)
En los agentes de diálogo esto es emulado, por ejemplo, a través de los sistemas de reconocimiento de voz, reconocimiento facial o de expresiones corporales.
Atención: Es la capacidad para aplicar voluntariamente una concentración sensorial hacia un estímulo determinado.
Memoria: Es la capacidad de procesar, almacenar y recuperar información de experiencias/estímulos pasados.
En agentes de diálogo esto puede ser emulado por el uso de variables contextuales como versión análoga a la memoria de corto plazo y base de datos entrenadas con aprendizaje automático como versión análoga a la memoria de largo plazo humana.
Los agentes de diálogo suelen ser entrenados con una base de datos de entrenamiento que puede contener (A) Conocimiento enciclopédico, (B) Corpuses.
Se llama corpus a una base de datos que contiene ejemplos reales del uso de una lengua dentro de un dominio en particular.
Cuando se trata de de una aproximación basada en conocimiento enciclopédicos se suele entrenar a nuestro agente con: Diccionarios o enciclopedias. (DBpedia, RAE, Wikipedia, Babelnet, Spanish Framenet, etc.)
Los procesos mentales superiores:
Pensamiento: Capacidad de elaborar, relacionar y construir ideas y representaciones de la realidad junto al conjunto de experiencias y estímulos asociadas a ella.
Lenguaje: Capacidad comunicativa de expresar el pensamiento a través de la palabra.
En función de poder comunicar agente y usuario precisamos del lenguaje y es por eso, una de las razones más importantes por las que existen las Tecnologías del lenguaje humano (TLH): (A) Procesamiento de lenguaje natural (PNL o NLP) / (B) Entendimiento de Lenguaje natural (ENL o NLU).
Las TLH facilitan la interrelación e intercambio informativo entre hombre y máquina así como la compresión y generación automática del lenguaje.
En resumen, cuando un agente recibe información esta se procesa emulando algunos de los procesos mentales del humano en función de devolver una respuesta que se adapte a dicho contexto. Dicho de otra manera, es una forma de toma de decisión consecuente al estímulo recibido, que tiene en cuenta la acción del otro agente humano. (Teoría de juegos)
Para que estas interacciones ocurran se precisa:
A- Una Interfaz: Chat, voz , etc…
B- Plataforma: Facebook messenger, web, escritorio, mobile app, etc…
C- Motor: DialogFlow, RasaNLU, Wit.AI, Watson, Chatfuel, etc…
¿Cómo se desarrolla un agente de diálogo virtual?
Primero que nada, al desarrollador de agentes de diálogo virtual se lo conoce normalmente como “botmaster”.
Al desarrollar un agente, es indispensable definir nuestra audiencia objetivo. Entender quienes van a ser nuestro nicho de usuario y en el marco donde estos se manejan. Una vez definido nuestro segmento de usuarios. Esto primero nos servirá para definir el “dominio” de nuestro agente.
Asimismo, es muy importante definir el problema que queremos resolverle a nuestros usuarios a través de nuestro agente. Esto nos permitirá definir el alcance de nuestro agente, lo cual remite al número y grado de resolución de consultas por parte de nuestros usuarios dentro del dominio ya establecido.
Existen dos enfoques de implementación de chatbots:
Transaccional (toma de pedidos, reservas, consultas típicas, recomendaciones — conductas autónomas , ia basada en necesidades ejem videojuego Los Sims) → Las interacciones son dirigidas, hay un dialogo asistido y dirigido para resolver objetivos en particular
Conversacional (Conversación abierta, debate, etc.) → Las interacciones son abiertas, el dominio es general, no tiene un objetivo necesariamente.
// IMPORTANTE!
Como desarrolladores de agentes de diálogo nuestro objetivo será evitar o minimizar la probabilidad de “ rotura” de nuestro agente. Se conoce como rotura cuando el usuario envía una consulta que nuestro agente falla en comprender, responde una consulta indeseada por el usuario o simplemente no sabe cómo responder.
Ante una rotura, normalmente un agente de diálogo responde con una respuesta de cobertura(o fallback response). Esta es una respuesta que comunica al usuario que su consulta se encuentra por momento fuera del alcance del agente.
Acá es importante emular asertividad y empatía para que el usuario…
Normalmente, al impulsar un proyecto de agentes de diálogo existen las siguientes etapas:
Investigación (User research)
Prototipado (Drafting)
Desarrollo (Development)
Testeo (Testing)
Retroalimentación (Feedback)
Publicación (Publication)
Ahora… veamos un poco la evolución historica de la ciencia cognitiva computacional:
B- Historia de la ciencia cognitiva y el internet:
1936 — Alan Turing y Alonzo Church presentan la tesis Church-Turing, considerado por la comunidad científica como el primer modelo funcional de inteligencia artificial.
1940 — Presentación del Modelo de neurona McCulloch y pitts
1948 — Se lleva a cabo el simposio de Hixon en donde Von Neumann introduce el concepto de procesamiento lineal de la información. Analogía computadora-cerebro. Se entiende a la mente como procesador de información.
1956 — Se lleva a cabo el simposio del MIT en el 11 de septiembre, momento en el cual nacen las ciencias cognitivas.
1960 — Se crea en Harvard el centro de estudios cognitivos
1966 — Se publica el bot ELIZA.
1972 — Se publica el bot PARRY
1980 — Tim Berners-Lee desarrolló el proyecto Enquire, un sistema que apunta a facilitar el intercambio de información entre investigadores.
1990 — Se desarrolla el HTML para concretar dicho intercambio.
1993 — Es creada y liberada la “world wide web”.
1994 — Mauldin publica el bot Julia.
1995 — Emerge la denominada web dinámica tras el surgimiento de lenguajes como php. (Altavista, Hotmail), ahora la web deja de ser estática.
1997 — Nacen los primeros blogs. Nueva forma de información.
1999 — Aparecen los primeros sistemas de CMS. (Blogger), nueva manera de generación de contenido. Aparecen los prosumidores.
2001 — Se publica el bot Eugene gootsman.
2001 — Con la intención de ordenar esta nueva información generada en la web. La W3C publica el artículo “la web semántica”. Aparecen iniciativas como “Linking open data” (sparql)
2006 — En ese año el parlamento europeo acepta el concepto de “3ra revolución industrial” o “revolución digital propuesto por el sociólogo Jeremy Rifkin. Este concepto remite a la emergente transformación de las tecnologías análogicas a tecnologías digitales.
2013 — El organismo noruego “SINTEF” publica el artículo “Big Data, for better or worse: 90% of world’s data generated over last two years.” , el cual pone en evidencia la magnitud de la generación de nueva información en la web.
2016 — En ese año, Klaus Schwab, presidente del Foro económico mundial propone el concepto de 4ta revolución industrial en donde se refiere a la progresiva y exponencial embebimiento de tecnologías emergentes y convergentes en el humano y sus entornos.
C- Desafíos
Estos son los desafios actuales en el campo de agentes de dialogo virtual:
Desafío de recursividad:
Este desafío remite a desafiar el enfoque lineal del procesamiento de la información y emular el proceso recursivo propio del pensamiento humano. Se puede entender por recursivo a la capacidad de metanálisis, es decir, la facultad del pensar sobre el pensar.
Desafío de grandes datos:
Este desafío se corresponde con entender cómo aprovechar, optimizar y procesar la información producida en los últimos tiempos de manera exponencial.
Desafío de ambigüedad del lenguaje:
Dado a que los agentes de diálogo se comunican a través del lenguaje, y este posee la propiedad de la ambigüedad, es necesario re-pensar cómo desarrollar agentes capaces de comprender ironías, etc.
Desafío de asertividad y computación afectiva:
Este desafío remite a la necesidad de desarrollar sistemas de diálogo que comprendan y modulen sus respuestas emocionales de manera adaptativa en sus interacciones con humanos.
Desafío de post-automatización:
Si el objetivo de los bots es automatizar tareas y cada vez se desarrollan más, ¿Qué haremos con la gente que se quede sin trabajo?
Conclusión:
Los agentes de diálogo virtual son piezas de software orientadas a automatizar tareas. Estos interactúan con usuarios a través de una interfaz de diálogo. Para procesar la información recibida por el usuario y responder a la misma se modelan computacionalmente procesos informáticos análogos a los procesos mentales básicos y superiores que estudia la ciencia cognitiva. Es por eso que argumentó que dicha disciplina es adecuada para abordar el desarrollo de estos agentes. La ciencia cognitiva nace luego de dos simposios importantes y luego el desarrollo de agentes de diálogo se ve afectado por los avances en el desarrollo de la web, y el contenido generado en ella. En respuesta a todos estos cambios históricos, existen diferentes desafíos en el área entre los que remarqué: (A) El desafío de recursividad, (B) Desafío de grandes datos, © Desafio de ambiguedad, (D) Desafio de asertividad y (E) desafio de post-automatización.
Bibliografía y Referencias:
Mauldin, Michael (1994), “ChatterBots, TinyMuds, and the
Turing Test: Entering the Loebner Prize Competition”,
Proceedings of the Eleventh National Conference on
Artificial Intelligence, AAAI Press, retrieved 2008–03–05.
Shevat, A. (2017). Designing bots: creating conversational
experiences. O’Reilly Media.
Deacon, Terrence William. (1997). The symbolic species :
the co-evolution of language and the brain. New York
:W.W. Norton.
Gazzaniga, M. S., Ivry, R. B., & Mangun, G. R. 1. (2014).
Cognitive neuroscience: The biology of the mind (Fourth
edition.). New York: W. W. Norton & Company, Inc…
A. Feigenbaum, E. (1995). Computers and thoughts.
McGraw-Hill, Inc.
Watson, J. B. (1913). «Psychology as the behaviorist views
it.» Psychological Review, 20, 158–177
Ramachandran V. (2003) The emerging mind. Renaissance Books.
Jacko, Julie. (2012) Human Computer Interaction Handbook:
Fundamentals, Evolving Technologies, and Emerging Applications,
Third Edition (Human Factors and Ergonomics)
Kaya, E. (2017) Bot Business 101: How to start, run & grow your
Bot / AI business Kindle Edition.
Nielsen (1994) Usability engineering. Morgan Kaufmann Publishers
Inc.
Palomar, M. Tecnologías de lenguaje humano aplicadas al
aprendizaje de segundas lenguas. Universidad de Alicante.
Peter Lang (2009), FrameNet Español: un análisis cognitivo del
léxico del español. Terminología y Sociedad del conocimiento.
Gardner H. (1988).La nueva ciencia de la mente. Buenos Aires:
Paidós.
De Vega M. (1984) Introducción a la Psicología Cognitiva.
Alianza Editorial. Madrid.
Johnson- Laird P. N.(1990).El ordenador y la mente.
Barcelona: Paidós.
Bot trends 2017 report (2017). Oratio. Recuperado de:
https://es.slideshare.net/3x14159265/bot-trends-2017
Holyoak, K.J.(2002). Psicología. En R. Willson &amp; F.
Keil (comp.). Enciclopedia MIT de Ciencias Cognitivas (pp
29–40). Madrid: Síntesis
Turing, Alan, On computable numbers, with an application
to the Entscheidungsproblem, Proceedings of the London
Mathematical Society, Series 2, 42 (1936), pp 230–265.
Church, A. 1932. A set of Postulates for the Foundation of
Logic. Annals of Mathematics, second series, 33, 346–366.
1936a. An Unsolvable Problem of Elementary Number
Theory. American Journal of Mathematics, 58, 345–363.
1936b. A Note on the Entscheidungsproblem. Journal of
Symbolic Logic, 1, 40–41.
1937a. Review of Turing 1936. Journal of Symbolic Logic,
2, 42–43.
1937b. Review of Post 1936. Journal of Symbolic Logic, 2,
43.
1941. The Calculi of Lambda-Conversion. Princeton:
Princeton University Press.
SWRL: A Semantic Web Rule Language Combining OWL
and RuleML. W3C. (2004)
Berners-Lee, Tim; James Hendler; Ora Lassila (May 17,
2001). “The Semantic Web”. Scientific American Magazine.
Retrieved March 26, 2008.
Bruner, (1988). A study of thinking. Transaction Publishers.
Miller, G. (1956). Magical number seven, Plus or Minus
Two. Some Limits on Our Capacity for Processing
Information. Harvard University.
Newell & Simmons (1972) La máquina de la teoría lógica.
J Neumann von (1958) The computer and the brain. Yale
Univ Press, New Haven
Berners-Lee, Tim; James Hendler; Ora Lassila (May 17,
2001). “The Semantic Web”. Scientific American Magazine.
Retrieved March 26, 2008.
The Digital Revolution (2006). UCSD.
Big Data, for better or worse: 90% of world’s data
generated over last two years. SINTEF. (2013)
Schwab, Klaus (January 11, 2016). The Fourth Industrial
Revolution. World Economic Forum. ISBN 1944835008.