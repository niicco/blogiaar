---
title: ¡Bienvenidos a IAAR blog!
date: 2016-11-23 23:32:49
tags:
cover_image: images/nanobots1.jpg

---

## El blog donde vas a encontrar artículos relacionados a Inteligencia Artificial desde una perspectiva filosófica, social y económica.
